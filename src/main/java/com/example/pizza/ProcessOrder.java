package com.example.pizza;

import com.itextpdf.text.DocumentException;

import java.io.FileNotFoundException;
import java.util.LinkedList;

public class ProcessOrder {
    private String order_id;
    private int total;
    private int total_tax;
    private int discount;
    private LinkedList<Product> products;
    private int[] quantities;
    public ProcessOrder(LinkedList<Product> products, int[] quantities) throws FileNotFoundException, DocumentException {
        this.products = products;
        this.quantities = quantities;
        mainLogic();
    }

    private void mainLogic() throws FileNotFoundException, DocumentException {
        countTotal();
        if(total > 10000){
            setDiscountByPercentage(20);
        }
        else
        {
            if(checkFreeBeverageDiscount() != -1){
                setItemDiscount(checkFreeBeverageDiscount(),100);
            }
            else
            {
                if(checkSpecialOfferDiscount() != -1){
                    setItemDiscount(checkSpecialOfferDiscount(),50);
                }
            }
        }
        Receipt rec = new Receipt(products,quantities,total,total_tax,discount);
        rec.generateReceipt();
    }

    private int checkFreeBeverageDiscount(){
        int beverageIndex = -1;
        int pizzas = 0;
        for(int i=0;i<products.size();i++){
            if(products.get(i).getId() < 100){
                pizzas += quantities[i];
            }
            else
            {
                if(beverageIndex == -1) {
                    beverageIndex = i;
                }
            }
            if(pizzas >= 2 && beverageIndex != -1){
                return beverageIndex;
            }
        }
        return -1;
    }

    private int checkSpecialOfferDiscount(){
        for(int i=0;i<products.size();i++){
            if(products.get(i).getPROMOTION_FLAG() == true){
                return i;
            }
        }
        return -1;
    }

    private void countTotal(){
        for(int i=0;i<products.size();i++)
        {
            total += products.get(i).getPrice() * quantities[i];
            total_tax += quantities[i] * products.get(i).getTaxValue();
        }
    }

    private void setDiscountByPercentage(int percentage){
        for(int i=0;i<products.size();i++)
        {
            total_tax += (int)(((quantities[i] * products.get(i).getTaxValue())*percentage)/100);
        }
        discount = (total * (100-percentage))/100;
        total -= discount;
    }

    private void setItemDiscount(int index, double percentage){
        discount = (int)((products.get(index).getPrice()*percentage)/100);
        total -= discount;
        total_tax -= (products.get(index).getVat()*percentage)/100;
    }
}
