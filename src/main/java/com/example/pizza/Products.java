package com.example.pizza;


public class Products extends Product{
    private static final int BEVERAGES_OFFSET = 100;
    private static final Product pizza_array[] = {
            new Product(1,"Margherita","sos pomidorowy, mozzarella",2000,5,false),
            new Product(2,"Margherita di Bufala","sos, pesto bazyliowe, mozzarella di bufala",2500,5,false),
            new Product(3,"Prosciutto e rucola","sos, szynka parmeńska, rukola, pomidorki koktajlowe, parmezan",3400,5,false),
            new Product(4,"Diavola","sos, ostre salami Spianata Piccante, jalapeno, czosnek, oliwki Kalamata, mozzarella",3200,5,true),
            new Product(5,"Quattro Formaggi","sos, mozzarella, gorgonzola, parmezan, wędzona mozzarella",3200,5,false),
            new Product(6,"Spianata","sos, salami Spianata Piccante, oliwki Kalamata, cebula, mozzarella, rukola",3200,5,true),
            new Product(7,"Spinaci","sos, duszony szpinak z czosnkiem , suszone pomidory , świeże liście szpinaku , mozzarella, ser Pecorino",3200,5,false),
            new Product(8,"Porcini","grzyby Porcini, czosnek, pietruszka, mozzarella, parmezan",3400,5,false),
            new Product(9,"Neapoli","sos, kapary, anchois, salami Spianata Piccante, mozzarella",3400,5,false),
            new Product(10,"Capriciosa","1/4 Spianata, 1/4 Quattro Formaggi, 1/4 Tonno, 1/4 Pollo, jajko , oregano",3500,5,false),
            new Product(11,"Pollo","sos, grillowany kurczak, cebula, papryka, oliwki Kalamata, mozzarella",3200,5,false),
            new Product(12,"Chorizo","sos, chorizo, oliwki Kalamata, cebula, mozzarella, rukola",3200,5,false),
            new Product(13,"Carbonara","sos, boczek pancetta, czosnek , parmezan , mozzarella, bazylia",3000,5,false)
    };
    private static final Product beverages_array[] = {
            new Product(BEVERAGES_OFFSET + 1,"Coca-cola 0,5l","",550,23,false),
            new Product(BEVERAGES_OFFSET + 2,"Coca-cola 0,33l","",400,23,false),
            new Product(BEVERAGES_OFFSET + 3,"Coca-cola Zero","",550,23,false),
            new Product(BEVERAGES_OFFSET + 4,"Fanta","",450,23,false),
            new Product(BEVERAGES_OFFSET + 5,"Sprite","",450,23,false),
            new Product(BEVERAGES_OFFSET + 6,"Fuze Tea","",450,23,false),
            new Product(BEVERAGES_OFFSET + 7,"Red Bull","",700,23,false),
            new Product(BEVERAGES_OFFSET + 8,"Kropla bezkidu - niegazowana","",350,23,false),
            new Product(BEVERAGES_OFFSET + 9,"Kropla bezkidu - gazowana","",350,23,false)
    };
    public Product getProduct(int id) throws IllegalArgumentException{
        if(id <= 0 || (id > Products.getPizza_array().length && id<101) || (id > Products.getBeverages_array().length+Products.getBeveragesOffset()) ){
            System.out.println(id + " " + Products.getPizza_array().length + " " + Products.getBeverages_array().length);
            throw new IllegalArgumentException("Niepoprawne ID");
        }
        if(id < 100)
            return pizza_array[id-1];
        else
            return beverages_array[id-1-BEVERAGES_OFFSET];
    }


    public static int getBeveragesOffset(){
        return BEVERAGES_OFFSET;
    }

    public static Product[] getPizza_array() {
        return pizza_array;
    }

    public static Product[] getBeverages_array() {
        return beverages_array;
    }
}





