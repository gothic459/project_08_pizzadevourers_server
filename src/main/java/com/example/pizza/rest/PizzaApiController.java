package com.example.pizza.rest;

import com.example.pizza.ProcessOrder;
import com.example.pizza.Product;
import com.example.pizza.Products;
import com.example.pizza.domain.dto.MenuDto;
import com.example.pizza.domain.dto.OrderDto;
import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.FileNotFoundException;
import java.util.LinkedList;


@Controller
@RequestMapping(value="/api")
public class PizzaApiController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PizzaApiController.class);

    @RequestMapping(value = "/pizza/order", method = RequestMethod.POST)
    public ResponseEntity<Void> testPizza(@RequestBody OrderDto pizzaDto) {
        try {
            sendOrder(pizzaDto);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (DocumentException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/pizza/products", method = RequestMethod.GET)
    public ResponseEntity<MenuDto> sendMenu(){
        final MenuDto menuDto = new MenuDto();
        return new ResponseEntity<>(menuDto,HttpStatus.OK);
    }


    public void sendOrder(OrderDto order) throws FileNotFoundException, DocumentException, IllegalArgumentException {
        LinkedList<Product> klient = new LinkedList<Product>();
        int ilosci[] = new int[order.getOrder().size()];
        Products products = new Products();

        for(int i=0;i<order.getOrder().size();i++)
        {
            ilosci[i] = order.getOrder().get(i).getQuantity();
            if(ilosci[i] == 0) {
                throw new IllegalArgumentException("Ilość nie może być równa zeru!");
            }
            klient.add(products.getProduct(order.getOrder().get(i).getId()));
        }
        ProcessOrder newOrder = new ProcessOrder(klient,ilosci);
    }
}
