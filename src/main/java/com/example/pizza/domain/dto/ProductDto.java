package com.example.pizza.domain.dto;

public class ProductDto {
    private int id;
    private String name;
    private String desc;
    private int price;
    private Boolean PROMOTION_FLAG;

    public ProductDto(){}

    public ProductDto(int id, String name,String desc, int price, Boolean PROMOTION_FLAG){
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.PROMOTION_FLAG = PROMOTION_FLAG;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDesc() { return desc; }

    public int getPrice(){
        return price;
    }

    public Boolean getPROMOTION_FLAG() {return PROMOTION_FLAG;}

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name = "+ name +
                ", price = " + price +
                '}';
    }
}