package com.example.pizza.domain.dto;

import java.util.List;

public class OrderDto {
    private List<ValuesDto> order;
    public OrderDto(){}
    public List<ValuesDto> getOrder(){
        return order;
    }

    @Override
    public String toString() {
        return "OrderDto{" +
                "values=" + order +
                '}';
    }
}


