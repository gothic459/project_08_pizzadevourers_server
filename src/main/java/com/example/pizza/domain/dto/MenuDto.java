package com.example.pizza.domain.dto;

import com.example.pizza.Products;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class MenuDto{
    private LinkedHashMap<String,ArrayList> menu = new LinkedHashMap<>();
    private ArrayList<ProductDto> pizzas = new ArrayList<>();
    private ArrayList<ProductDto> beverages = new ArrayList<>();
    public MenuDto(){
        for(int i=0;i<Products.getPizza_array().length;i++)
        {
            pizzas.add(Products.getPizza_array()[i].getProductDtoFromProduct());
        }
        for(int i=0;i<Products.getBeverages_array().length;i++)
        {
            beverages.add(Products.getBeverages_array()[i].getProductDtoFromProduct());
        }
        menu.put("pizzas",pizzas);
        menu.put("beverages",beverages);
    }

    public LinkedHashMap<String,ArrayList> getMenu(){
        return menu;
    }

    @Override
    public String toString() {
        return "PizzaDto{" +
                "values=" + menu +
                '}';
    }
}