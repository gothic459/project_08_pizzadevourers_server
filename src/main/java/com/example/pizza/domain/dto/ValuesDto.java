package com.example.pizza.domain.dto;

public class ValuesDto {
    private int id;
    private int quantity;
    private int PROMOTION_FLAG;

    public ValuesDto(){}

    public ValuesDto(int id, int quantity, int PROMOTION_FLAG){
        this.id = id;
        this.quantity = quantity;
        this.PROMOTION_FLAG = PROMOTION_FLAG;
    }

    public int getId() {
        return id;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPROMOTION_FLAG() { return PROMOTION_FLAG; }

    @Override
    public String toString() {
        return "Values{" +
                "id=" + id +
                ", quantity=" + quantity +
                '}';
    }
}