package com.example.pizza;

import com.example.pizza.domain.dto.ProductDto;

public class Product {
    private int id;
    private String name;
    private String desc;
    private int price;
    private Boolean PROMOTION_FLAG;
    private final double vat;
    public Product(){
        this.id = 0;
        this.name = "";
        this.price = 0;
        this.vat = 0;
    }
    public Product(int id, String name,String desc, int price, int vat, Boolean PROMOTION_FLAG){
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.price = price;
        this.vat = vat;
        this.PROMOTION_FLAG = PROMOTION_FLAG;
    }

    public ProductDto getProductDtoFromProduct(){
        return new ProductDto(this.id,this.name,this.desc,this.price,this.PROMOTION_FLAG);
    }

    public double getTaxValue(){
        return (price * vat) / 100;
    }

    public Boolean getPROMOTION_FLAG() { return PROMOTION_FLAG; }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getDesc() {return desc;}

    public int getId() {
        return id;
    }

    public double getVat() {
        return vat;
    }
}